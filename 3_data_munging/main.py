class Munging:

    def __init__(self, filePath):
        self.info = [row.strip().split() for row in open(filePath).readlines()]

    def min_diff(self, get, col1, col2):

        self.get = get
        self.object1 = col1
        self.object2 = col2
        smallest = 1000

        for data in self.info[1:]:
            try:
                diff = float(data[self.object1].strip('*')) - \
                    float(data[self.object2].strip('*'))
                if abs(diff) < smallest:
                    smallest = abs(diff)
                    min = data[self.get]
            except:
                pass

        return min
