from main import Munging

team = 1
goal_for = 6
goal_against = 8
football = Munging('football.dat')
print("Team with the smallest difference in ‘for’ and ‘against’ goals = ",
      football.min_diff(team, goal_for, goal_against))
